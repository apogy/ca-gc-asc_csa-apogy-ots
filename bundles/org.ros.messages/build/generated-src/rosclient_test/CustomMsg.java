package rosclient_test;

public interface CustomMsg extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "rosclient_test/CustomMsg";
  static final java.lang.String _DEFINITION = "int32 intVar\nstring stringVar\n";
  int getIntVar();
  void setIntVar(int value);
  java.lang.String getStringVar();
  void setStringVar(java.lang.String value);
}
