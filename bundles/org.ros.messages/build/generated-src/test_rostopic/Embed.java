package test_rostopic;

public interface Embed extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "test_rostopic/Embed";
  static final java.lang.String _DEFINITION = "Simple simple\nArrays arrays\n";
  test_rostopic.Simple getSimple();
  void setSimple(test_rostopic.Simple value);
  test_rostopic.Arrays getArrays();
  void setArrays(test_rostopic.Arrays value);
}
