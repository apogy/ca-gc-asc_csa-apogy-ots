package clearpath_base;

public interface Joy extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/Joy";
  static final java.lang.String _DEFINITION = "float32[] axes\nint32[] buttons\n";
  float[] getAxes();
  void setAxes(float[] value);
  int[] getButtons();
  void setButtons(int[] value);
}
