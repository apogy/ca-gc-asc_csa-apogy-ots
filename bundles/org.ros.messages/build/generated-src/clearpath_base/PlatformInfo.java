package clearpath_base;

public interface PlatformInfo extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/PlatformInfo";
  static final java.lang.String _DEFINITION = "Header header\nstring model\nint8 revision\nuint32 serial\n";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  java.lang.String getModel();
  void setModel(java.lang.String value);
  byte getRevision();
  void setRevision(byte value);
  int getSerial();
  void setSerial(int value);
}
