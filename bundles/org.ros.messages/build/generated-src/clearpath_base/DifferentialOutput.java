package clearpath_base;

public interface DifferentialOutput extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/DifferentialOutput";
  static final java.lang.String _DEFINITION = "Header header\nfloat64 left\nfloat64 right\n";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  double getLeft();
  void setLeft(double value);
  double getRight();
  void setRight(double value);
}
