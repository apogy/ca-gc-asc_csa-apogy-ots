package clearpath_base;

public interface ClearpathRobot extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/ClearpathRobot";
  static final java.lang.String _DEFINITION = "string name\nstring model\nint32 platform_revision\nuint32 serial\nint32[2] horizon_version\nint32[2] firmware_version\nint32 firmware_revision\nstring write_date\n";
  java.lang.String getName();
  void setName(java.lang.String value);
  java.lang.String getModel();
  void setModel(java.lang.String value);
  int getPlatformRevision();
  void setPlatformRevision(int value);
  int getSerial();
  void setSerial(int value);
  int[] getHorizonVersion();
  void setHorizonVersion(int[] value);
  int[] getFirmwareVersion();
  void setFirmwareVersion(int[] value);
  int getFirmwareRevision();
  void setFirmwareRevision(int value);
  java.lang.String getWriteDate();
  void setWriteDate(java.lang.String value);
}
