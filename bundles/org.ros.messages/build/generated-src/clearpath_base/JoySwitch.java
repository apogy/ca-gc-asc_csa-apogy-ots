package clearpath_base;

public interface JoySwitch extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/JoySwitch";
  static final java.lang.String _DEFINITION = "string robot_id\nuint8 attach\nstring joystick\n";
  java.lang.String getRobotId();
  void setRobotId(java.lang.String value);
  byte getAttach();
  void setAttach(byte value);
  java.lang.String getJoystick();
  void setJoystick(java.lang.String value);
}
