package clearpath_base;

public interface SystemStatus extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/SystemStatus";
  static final java.lang.String _DEFINITION = "Header header\nuint32 uptime\nfloat64[] voltages\nfloat64[] currents\nfloat64[] temperatures\n";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  int getUptime();
  void setUptime(int value);
  double[] getVoltages();
  void setVoltages(double[] value);
  double[] getCurrents();
  void setCurrents(double[] value);
  double[] getTemperatures();
  void setTemperatures(double[] value);
}
