package clearpath_base;

public interface PowerSource extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/PowerSource";
  static final java.lang.String _DEFINITION = "float32 charge\nint16 capacity\nbool present\nbool in_use\nuint8 description\n";
  float getCharge();
  void setCharge(float value);
  short getCapacity();
  void setCapacity(short value);
  boolean getPresent();
  void setPresent(boolean value);
  boolean getInUse();
  void setInUse(boolean value);
  byte getDescription();
  void setDescription(byte value);
}
