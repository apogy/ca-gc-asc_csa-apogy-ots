package clearpath_base;

public interface Magnetometer extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/Magnetometer";
  static final java.lang.String _DEFINITION = "Header header\nfloat64 x\nfloat64 y\nfloat64 z";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  double getX();
  void setX(double value);
  double getY();
  void setY(double value);
  double getZ();
  void setZ(double value);
}
