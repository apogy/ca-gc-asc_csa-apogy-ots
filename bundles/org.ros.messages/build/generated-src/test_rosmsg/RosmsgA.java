package test_rosmsg;

public interface RosmsgA extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "test_rosmsg/RosmsgA";
  static final java.lang.String _DEFINITION = "int32 a\n";
  int getA();
  void setA(int value);
}
