package test_rosmsg;

public interface RossrvA extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "test_rosmsg/RossrvA";
  static final java.lang.String _DEFINITION = "int32 areq\n---\nint32 aresp\n";
}
