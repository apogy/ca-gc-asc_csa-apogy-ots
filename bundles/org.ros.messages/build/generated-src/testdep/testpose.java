package testdep;

public interface testpose extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "testdep/testpose";
  static final java.lang.String _DEFINITION = "geometry_msgs/Pose pose\n";
  geometry_msgs.Pose getPose();
  void setPose(geometry_msgs.Pose value);
}
